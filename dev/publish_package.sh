#!/bin/sh

if [ -z `git tag -l 'v[0-9].[0-9].[0-9]'` ]
  then
    echo "HEAD is missing version tag"
    exit 1
fi

python3 -m twine upload -u "$USERNAME" -p "$PASSWORD" dist/*

#!/bin/bash

project_id=9150401

set -e

if [[ -z "$1" ]] || [[ -z "$2" ]]
  then
    echo "Run with arguments <version> <gitlab-token>"
    exit 1
fi

echo "Are you on clean develop branch? y/n"
read answer
if [[ "$answer" = 'n' ]]
  then
    echo "Try again!"
    exit 1
fi

branch="$(git rev-parse --abbrev-ref HEAD)"
if [[ "$branch" != 'develop' ]]
  then
    echo "Wrong branch!"
    exit 1
fi

version="$(bumpversion "$1" --list | grep 'new_version' | cut -d '=' -f 2)"
tag="v$version"
release_branch="$version"
release_commit_message="Release $version"

git checkout -b "$version"

git add setup.py
git add .bumpversion.cfg
git commit -m "$release_commit_message"
git tag "$tag"
git push origin "$release_branch" --tags
git checkout develop

curl --data "source_branch=${release_branch}&target_branch=master&title=${release_commit_message}&remove_source_branch=true&allow_collaboration=true&approvals_before_merge=5" --header "PRIVATE-TOKEN: $2" "https://gitlab.com/api/v4/projects/$project_id/merge_requests"
curl --data "source_branch=${release_branch}&target_branch=develop&title=${release_commit_message}&remove_source_branch=true&allow_collaboration=true&approvals_before_merge=5" --header "PRIVATE-TOKEN: $2" "https://gitlab.com/api/v4/projects/$project_id/merge_requests"
